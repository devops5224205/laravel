# Stage 1: Build Laravel application
FROM composer:2 AS composer

WORKDIR /app

COPY . .

RUN composer install --no-dev --optimize-autoloader

# Build frontend assets if needed
# RUN npm install && npm run production

# Stage 2: Build Nginx container
FROM nginx:alpine

# Copy built Laravel application from previous stage
COPY --from=composer /app /var/www/html

# Copy nginx configuration
COPY nginx.conf /etc/nginx/nginx.conf

# Expose port 80
EXPOSE 80
